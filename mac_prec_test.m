%define our machine precsion
epsilon = mac_prec();

%Define v1,v2,v3 with the epsilon value being some multipe of machine
%precision.
epsilon = epsilon*1e8;
v1 = [1 epsilon 0 0]';
v2 = [1 0 epsilon 0]';
v3 = [1 0 0 epsilon]';


%find the magnitude of v1,v2,v3
mag_v1 = norm(v1);
mag_v2 = norm(v2);
mag_v3 = norm(v3);

%Reports to the user if the magnitude of v1 is 1 or not.
if mag_v1 == 1
    fprintf('Magnitude is 1 \n')
else
    fprintf('Magnitude is not 1 \n')
end

%if the dot products of two vectors is 0 they are orthogonal. If the dot
%product of all vectors with each other is 0 they are all orthogonal.
if dot(v1,v2) == 0 & dot(v1,v3) == 0 & dot(v2,v3) == 0
    fprintf('v1, v2, and v3, are orthogonal. \n')
else
    fprintf('v1, v2, and v3, are not orthogonal. \n')
endJoshua Arenson
