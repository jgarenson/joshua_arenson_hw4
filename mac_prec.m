function numprec = mac_prec()
%Calculates machine precision, assigns it to variable numprec.

%Defines if we are looking for double precision or single. Uncommented
%lines is the one we're looking for.
numprec=double(1.0); % Define 1.0 with double precision
% numprec=single(1.0); % Define 1.0 with single precision

%The condition is violated when numprec < machine precision is reached since the
%machine will round 1+numprec to 1.
while(1 < 1 + numprec)
    numprec=numprec*0.5;
end
%We need to double the value because the last case is below machine
%precision.
numprec=numprec*2;
Joshua Arenson
