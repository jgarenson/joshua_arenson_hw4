function [Q R] = CGS(A)
%Applies a classic Gram-Schmidt algorithim to the input matrix A breaking
%it into it's QR facorization. Q is a mxn matrix and R is a upper
%triangular nxn matrix.

%Identify size of input matrix, and initialize the sizes of Q and R.
[m n] = size(A);
Q = zeros(m,n);
R = zeros(n,n);

%Gram-Schmidt Algorithm. Each iterate of j produces one of the column
%vectors of Q and R. 
for j = 1:n
    v = A(:,j);
    for i = 1:j-1
        R(i,j) = Q(:,i)'*A(:,j);
        v = v - R(i,j)*Q(:,i);
    end
    R(j,j) = norm(v);
    Q(:,j) = v/R(j,j);
end

%It should be noted that there is some minor difference between the MATLAB
%inbuilt gram-schmidt and the one provided by the book. The MATLAB command
%qr(A) produces Q and R that are negative of the values produced using the
%code provided on page 51. It should be noted that this does NOT violate
%uniqueness of the QR-factorization since the -1 constant infront of both Q
%AND R can be removed and cancel each other out. 
Joshua Arenson
