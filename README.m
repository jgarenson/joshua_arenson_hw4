%HW #4 Computation Problem (#7) README.


%CGS.m is the classic gram-schmidt method, for problem 7.a

%mac_prec.m is used to determine the machine precision of the computer
%running the program. Simply call (with no inputs) to get our machine
%precision, can be saved to a variable.

%mac_prec_test.m is used to test various values epsilon near machine
%precision. This test sees how the magnitude of vectors change under
%various epsilon. Joshua Arenson
Joshua Arenson
